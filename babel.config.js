module.exports = function(api) {
  api.cache(true);
  return {
    presets: [['babel-preset-expo', { lazyImports: false}]],
    plugins: [
      "react-native-reanimated/plugin",
    ],
  };
};
