import React, { useState, useEffect } from 'react';
import Colors from "../constants/Colors";
import Layout from "../constants/Layout";
import {View, Text, FlatList, StyleSheet, TouchableOpacity, ImageBackground, Dimensions} from "react-native";
import transactionService from "../services/transactionService"
import topicColors from "../constants/TopicColors";
import { LocalizationContext } from "../../LocalizationContext";
import * as Icons from '../constants/TopicIcons';

const TopicList = ({ navigation }) => {
    const width = Dimensions.get('window').width;
    let [topics, setTopics] = useState([]);
    let { locale } = React.useContext(LocalizationContext);

    useEffect(() => {
        //console.log('width => ' + width );
        transactionService.getTopics(locale,results => setTopics(results));
    }, [locale]);

    function getTopicStyle(id) {
        return {
            flexDirection: 'column',
            margin: 5,
            borderRadius: 20,
            backgroundColor: topicColors.getTopicColorById(id),
        }
    }

    function getTopicTitleStyle() {
        return {
            fontSize: getTopicTitleSize(),
            color: Colors.textDefaultColor,
            fontWeight: '700',
            fontFamily: 'nunito-bold',
            paddingRight : 8
        }
    }

    function getIconDimensions() {
        if (Layout.isVerySmallDevice) {
            return 90;
        } else if (Layout.isSmallDevice) {
            return 100;
        } else if (Layout.isRegularDevice) {
            return 108;
        } else {
            return 115;
        }
    }

    function getTopicTitleSize() {
        if (Layout.isVerySmallDevice) {
            return 9;
        } else if (Layout.isSmallDevice) {
            return 10;
        } else if (Layout.isRegularDevice) {
            return 11;
        } else {
            return 12;
        }
    }

    function getBottomRelative() {
        if (Layout.isVerySmallDevice) {
            return -60;
        } else if (Layout.isSmallDevice) {
            return -65;
        } else if (Layout.isRegularDevice) {
            return -78;
        } else {
            return -78;
        }
    }

    function getIconStyle() {
        return {
            width: getIconDimensions(),
            height: getIconDimensions(),
            resizeMode: 'center'
        }
    }

    let listViewItemSeparator = () => {
        return (
            <View />
        );
    };

    let listItemView = (item) => {
        return (
            <View
                style={getTopicStyle(item.id)}
            >

                <TouchableOpacity
                    onPress={ ()=> navigation.navigate('SubTopics', { subtopicId: item.id, title: item.text}) }>
                    <ImageBackground
                        style={getIconStyle()}
                        source={Icons.topicIcons.get(item.id)}>
                        <View style={{position: 'absolute', top: 0, left: 12, right: 0, bottom: getBottomRelative(), justifyContent: 'center', alignItems: 'stretch'}}>
                            <Text
                                style={getTopicTitleStyle()}>
                                {item.text}
                            </Text>
                        </View>
                    </ImageBackground>

                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.container}>
                <FlatList
                    showsVerticalScrollIndicator ={false}
                    showsHorizontalScrollIndicator={false}
                    data={topics}
                    ItemSeparatorComponent={listViewItemSeparator}
                    numColumns={3}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => listItemView(item)}
                />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});

export default TopicList;