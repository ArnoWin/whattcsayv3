import React, { useState, useEffect } from 'react';
import Colors from "../constants/Colors";
import Layout from "../constants/Layout";
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import { LocalizationContext } from "../../LocalizationContext";
import transactionService from "../services/transactionService";
import topicColors from "../constants/TopicColors";


const SubTopicList = ({ navigation, route }) => {

    let [topics, setTopics] = useState([]);
    let { locale } = React.useContext(LocalizationContext);

    useEffect(() => {
        transactionService.getSubTopics(route.params.subtopicId, locale,results => setTopics(results));
    }, [locale]);

    function getSubtopicWidth() {
        if (Layout.isVerySmallDevice) {
            return 280;
        } else if (Layout.isSmallDevice) {
            return 330;
        } else if (Layout.isRegularDevice) {
            return 368;
        } else {
            return 368;
        }
    }

    function getSubtopicStyle(id) {
        return {
            flex: 1,
            borderRadius: 100,
            width: getSubtopicWidth(),
            height: 56,
            backgroundColor: topicColors.getTopicColorById(id),
            justifyContent: 'center'
        }
    }

    let listViewItemSeparator = () => {
        return (
            <View
                style={{
                    width: '100%',
                    height: 8
                }}/>
        );
    };

    let listItemView = (item) => {
        return (
            <View
                style={getSubtopicStyle(item.topic_id)}
                key={item.id}
            >
                <TouchableOpacity
                    onPress={ ()=> navigation.navigate('Words', { subtopicId: item.id, title: item.text}) }>
                    <Text style={styles.subtopicText}>
                        {item.text}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <FlatList
                showsVerticalScrollIndicator ={false}
                showsHorizontalScrollIndicator={false}
                data={topics}
                ItemSeparatorComponent={listViewItemSeparator}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => listItemView(item)}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 24
    },
    subtopicText: {
        fontSize: 18,
        fontWeight: '800',
        color: Colors.textDefaultColor,
        paddingHorizontal: 30,
        fontFamily: 'nunito-extrabold',
        textAlign: 'left'
    }
});

export default SubTopicList;