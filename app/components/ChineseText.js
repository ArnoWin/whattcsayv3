import React from "react";
import Colors from "../constants/Colors";
import { Menu, MenuOption, MenuOptions, MenuTrigger } from "react-native-popup-menu";
import { Clipboard, StyleSheet, Text, View } from "react-native";
import { LocalizationContext } from "../../LocalizationContext";

const ChineseText = ({ hanzi }) => {
    const { t } = React.useContext(LocalizationContext);

    return (
        <View style={styles.container}>
            <Menu>
                <MenuTrigger triggerOnLongPress={true}>
                    <Text
                        style={styles.hanziStyle}>
                        {hanzi}
                    </Text>
                </MenuTrigger>
                <MenuOptions optionsContainerStyle={styles.menuOptionsSytle}>
                    <MenuOption onSelect={() => Clipboard.setString(hanzi)}  >
                        <Text style={{ textAlign:'center', color:'white' }}>{t('copy')}</Text>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        </View>
    );
}

export default ChineseText

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingTop: 8,
        paddingLeft: 44,
        paddingRight: 16,
        paddingBottom: 16
    },
    hanziStyle : {
        color: Colors.chineseColor,
        fontSize: 14,
        fontFamily: 'noto-sans-ui'
    },
    menuOptionsSytle : {
        width: 75,
        opacity: 0.5,
        backgroundColor : 'grey',
        borderRadius: 8,
        marginTop: 25,
        marginLeft: 20
    }
});