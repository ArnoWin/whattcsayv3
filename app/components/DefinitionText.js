import React from "react";
import Colors from "../constants/Colors";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from "react-native-popup-menu";
import {Clipboard, StyleSheet, Text, View} from "react-native";
import {LocalizationContext} from "../../LocalizationContext";
import ReportBug from "./ReportBug";

const DefinitionText = ({item}) => {

    const {t} = React.useContext(LocalizationContext);

    function capitalizeFirstLetter(text) {
        return text.charAt(0).toUpperCase() + text.slice(1);
    }

    return (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View style={styles.container}>
            <Menu>
                <MenuTrigger triggerOnLongPress={true}>
                        <Text
                            style={styles.definition}>
                            {capitalizeFirstLetter(item.def)}
                        </Text>

                </MenuTrigger>
                <MenuOptions optionsContainerStyle={styles.menuOptionsSytle}>
                    <MenuOption onSelect={() => Clipboard.setString(item.def)}>
                        <Text style={{textAlign: 'center', color: 'white'}}>{t('copy')}</Text>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        </View>
        <View><ReportBug item={item}></ReportBug></View>
        </View>
    );
}

export default DefinitionText

const styles = StyleSheet.create({
    container: {
        flex: 0.95,
        flexDirection: 'row',
        paddingTop: 16,
        paddingHorizontal: 16,
    },
    definition: {
        color: Colors.blackColor,
        fontSize: 16,
        fontStyle: 'normal',
        fontFamily: 'nunito-extrabold',
    },
    menuOptionsSytle: {
        width: 75,
        backgroundColor: 'grey',
        borderRadius: 8,
        marginTop: 25,
        marginLeft: 20
    }
});
