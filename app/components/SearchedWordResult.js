import { FlatList, View } from "react-native";
import React from "react";
import SearchedWordEntry from "./SearchedWordEntry";

const SearchedWordList = ({ navigation, route, words }) => {

    const listViewItemSeparator = () => {
        return (
            <View
                style={{
                    flex: 1,
                    paddingVertical: 8,
                }}/>
        );
    };

    let listItemView = (item) => {
        return (
            <SearchedWordEntry navigation={navigation} route={route} word={item} />
        );
    };

    return (
        <View style={{
            flex: 1,
            marginVertical: 20,
            marginHorizontal: 20}}>
            <FlatList
                showsVerticalScrollIndicator ={true}
                showsHorizontalScrollIndicator={false}
                data={words}
                ItemSeparatorComponent={listViewItemSeparator}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => listItemView(item)}
            />
        </View>
    );
}

export default SearchedWordList;
