import React, { useEffect} from "react";
import { Audio, InterruptionModeAndroid, InterruptionModeIOS } from 'expo-av'
import {Clipboard, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from "../constants/Colors";
import * as Assets from '../constants/PengimAssets'
import {Menu, MenuOption, MenuOptions, MenuTrigger} from "react-native-popup-menu";
import {LocalizationContext} from "../../LocalizationContext";
import * as Haptic from "expo-haptics";

const PengimText = ({ pengim, soundUrl }) => {

    let isPlayed = false;
    const { t } = React.useContext(LocalizationContext);

    useEffect(() => {
        Audio.setAudioModeAsync({
                allowsRecordingIOS: false,
                interruptionModeIOS: InterruptionModeIOS.MixWithOthers,
                playsInSilentModeIOS: true,
                interruptionModeAndroid: InterruptionModeAndroid.DoNotMix,
                shouldDuckAndroid: true,
                staysActiveInBackground: true,
                playThroughEarpieceAndroid: false
            }
        );

    }, []);

    async function playSound(){
        if (hasSoundFile(soundUrl)) {
            try {
                await Audio.setIsEnabledAsync(true);
                const soundObject = new Audio.Sound();
                await soundObject.loadAsync(Assets.pengimAssets.get(soundUrl));
                const status = await soundObject.playAsync();
                isPlayed = true;
                setTimeout(() => {
                    soundObject.unloadAsync();
                    isPlayed = false;
                }, status.playableDurationMillis + 1000)
                await Haptic.selectionAsync();
            } catch (error) {
                console.log('Error in playSound with soundUrl: ' + soundUrl + ' - Error is :' + error.message);
            }
        }
    }

    function hasSoundFile(soundUrl) {
        return (soundUrl != undefined && soundUrl.toLowerCase().endsWith('.mp3'))
    }

    return (
        <View style={styles.container}>
            <View>
                <Menu>
                    <MenuTrigger>
                        <MaterialCommunityIcons
                            name='content-copy'
                            size={15}
                            color={Colors.tabIconDefault}
                        />
                    </MenuTrigger>
                    <MenuOptions optionsContainerStyle={styles.menuOptionsSytle}>
                        <MenuOption onSelect={() => Clipboard.setString(pengim)}  >
                            <Text style={{ textAlign:'center', color: 'white' }}>{t('copy')}</Text>
                        </MenuOption>
                    </MenuOptions>
                </Menu>
            </View>
            <View style={{ flex : 1}}>
            <TouchableOpacity onPress={playSound}>
                <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                    { hasSoundFile(soundUrl) ? <View style={{ paddingHorizontal: 10}}><Image style={styles.speakerIcon} source={require('../../assets/images/sound.png')}></Image></View> : <View style={{ paddingLeft: 10}}/>}
                <Text style={styles.pengimText}>
                    {pengim}
                </Text>
                    <View><Text /></View>
                </View>
            </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 16,
        paddingRight: 28,
    },
    pengimText : {
        color: Colors.pengimColor,
        fontSize: 14,
        fontFamily: 'nunito-regular'
    },
    menuOptionsSytle : {
    width: 75,
        backgroundColor : 'grey',
        borderRadius: 8,
        marginTop: 25,
        marginLeft: 20
    },
    speakerIcon: {
        paddingHorizontal: 10,
        width: 15,
        height: 14,
        resizeMode: 'stretch',
        paddingRight: 10
    }
});

export default PengimText
