import React from "react";
import {StyleSheet, Text, View, TouchableOpacity} from "react-native";
import PengimText from "./PengimText";
import DefinitionText from "./DefinitionText";
import ChineseText from "./ChineseText";
import topicColors from "../constants/TopicColors";
import GrammarText from "./GrammarText";
import EtymologyText from "./EtymologyText";

const SearchedWordEntry = ({ navigation, route, word }) => {

    function getSubtopicStyle(id) {
        return {
            borderRadius: 100,
            padding: 8,
            paddingHorizontal: 15,
            marginLeft: 16,
            alignSelf: 'flex-start',
            backgroundColor: topicColors.getTopicColorById(id),
        }
    }

    function displayGrammar() {
        if (word.part_of_speech == '' && word.register == '') {
            return false;
        } else {
            return true;
        }
    }

    function displayEtymology() {
        if (word.origin == '' && word.dialect == '') {
            return false;
        } else {
            return true;
        }
    }

    return (
        <View style={styles.container}
            key={word.definition_id}>
            <DefinitionText item={word} />
            {displayGrammar() ? <GrammarText partOfSpeech={word.part_of_speech} register={word.register}/> : <View /> }
            <View style={{ flex : 1, marginTop: 10}}>
                <PengimText pengim={word.romanization} soundUrl={word.sound_files}></PengimText>
            </View>
            {displayEtymology() ? <EtymologyText origin={word.origin} dialect={word.dialect}/> : <View />}
            <ChineseText hanzi={word.chinese_char} />
            <TouchableOpacity  onPress={ ()=> navigation.navigate('Words', { subtopicId: word.subtopic_id, title: word.subtopic}) }>
                <View style={getSubtopicStyle(word.topic_id)}>
                    <Text
                        style={styles.subtopicText}
                       >
                        {word.subtopic}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 8,
        backgroundColor: 'white',
        paddingBottom: 16,
    },
    subtopicText : {
        color: 'white',
        fontSize: 12,
        fontFamily: 'nunito-bold',
        fontWeight:'700'
    }
});

export default SearchedWordEntry
