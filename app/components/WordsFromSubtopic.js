import { FlatList, StyleSheet, View , Text} from "react-native";
import React from "react";
import PengimText from "./PengimText";
import DefinitionText from "./DefinitionText";
import ChineseText from "./ChineseText";
import GrammarText from "./GrammarText";
import EtymologyText from "./EtymologyText";

const WordList = ({ words }) => {

    const listViewItemSeparator = () => {
        return (
            <View
                style={{
                    flex: 1,
                    width: '100%',
                    marginVertical: 8
                }}/>
        );
    };

    let listItemView = (item) => {
        function displayGrammar() {
            if (item.part_of_speech == '' && item.register == '') {
                return false;
            } else {
                return true;
            }
        }

        function displayEtymology() {
            if (item.origin == '' && item.dialect == '') {
                return false;
            } else {
                return true;
            }
        }
        return (
            <View style={styles.elementContainer} key={item.definition_id}>
                <DefinitionText item={item} />
                {displayGrammar() ? <GrammarText partOfSpeech={item.part_of_speech} register={item.register}/> : <View /> }
                <View style={{ flex: 1, marginTop: 10}}>
                    <PengimText pengim={item.romanization} soundUrl={item.sound_files}></PengimText>
                </View>
                {displayEtymology() ? <EtymologyText origin={item.origin} dialect={item.dialect}/> : <View />}
                <ChineseText hanzi={item.chinese_char} />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <FlatList
                showsVerticalScrollIndicator ={true}
                showsHorizontalScrollIndicator={false}
                data={words}
                ItemSeparatorComponent={listViewItemSeparator}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => listItemView(item)}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 24,
        paddingHorizontal: 20,
    },
    elementContainer: {
        backgroundColor : 'white',
        borderRadius : 10,
    }
});

export default WordList;
