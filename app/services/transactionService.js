import databaseService from "./databaseService";

const db = databaseService.getDatabase();

class TransactionService {

    getTopics(locale, callback) {
        const query = databaseService.getTopicsQuery(locale);
        this.getTransaction(query, callback);
    }

    getSubTopics(id, locale, callback) {
        const query = databaseService.getSubTopicsQuery(id, locale);
        this.getTransaction(query, callback);
    }

    getAutoComplete(locale, callback) {
        const query = databaseService.getAutocompleteQuery(locale);
        this.getTransaction(query, callback);
    }

    getLikeQuery(likeCriteria, locale, callback) {
        const query = databaseService.getLikeQuery(likeCriteria, locale);
        this.getTransaction(query, callback);
    }

    getWords(subtopicId, locale, callback){
        const query = databaseService.getWordsQuery(subtopicId, locale);
        this.getTransaction(query,callback);
    }

    getTransaction(query, callback) {
        db.transaction(
            tx => {
                tx.executeSql(query, [], function (tx, results) {
                        let temp = [];
                        for (let i = 0; i < results.rows.length; i++) {
                            temp.push(results.rows.item(i));
                        }
                        callback(temp);
                    }
                );
            });
    }
}

const transactionService = new TransactionService();
export default transactionService;