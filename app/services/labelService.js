import * as React from "react";
import i18n from 'i18n-js';

const en = {
    french: 'French',
    english: 'English',
    search: 'Dictionary',
    topics: 'Topics',
    topicList: 'Topics',
    subtopics: 'Subtopics',
    words: 'Dictionary',
    searchScreen_label: 'Search in the dictionary',
    topicScreen_title: 'Topics List',
    wordScreen_title: 'Words list',
    copy: 'Copy',
    reportPopinTitle: 'Report a bug',
    definition: 'Definition',
    chinese_char: 'Chinese',
    romanization: 'Romanization',
    partOfSpeech: 'Part of speech',
    dialect: 'Dialect',
    origin: 'Origin',
    register: 'Register',
    comment:'Comment',
    reportButton: 'Report',
    cancelButton: 'Cancel',
    typeOfBug: 'Type of bug',
    reported: 'Bug report sent',

    noun: 'Noun',
    surname: 'Surname',
    pronoun: 'Pronoun',
    verb: 'Verb',
    auxiliary_verb: 'Auxiliary Verb',
    adjective: 'Adjective',
    adverb: 'Adverb',
    number_word: 'Number word',
    measure_word: 'Measure word',
    interjection: 'Interjection',
    onomatopoeia: 'Onomatopoeia',
    conjunction: 'Conjunction',
    preposition: 'Preposition',
    particle: 'Particle',
    expression: 'Expression',
    structure: 'Structure',
    sentence: 'Sentence',
    determiner: 'Determiner',

    vulgar: 'Vulgar',
    colloquial: 'Colloquial',
    polite: 'Polite',
    literary: 'Literary',
    formal: 'Formal',
    dated: 'Dated',

    Overseas_TC: 'Overseas-TC',
    MSI_TC: 'MSI-TC',
    Malay_TC: 'Malay-TC',
    Singaporean_TC: 'Singaporean-TC',
    Indonesian_TC: 'Indonesian-TC',
    Cambodian_TC: 'Cambodian-TC',
    Vietnamese_TC: 'Vietnamese-TC',
    Diosuan: 'Diosuan',
    Husian: 'Husian',
    Gik_ion: 'Gik-ion',
    Dio_ion: 'Dio-ion',
    Pouleng: 'Pouleng',
    Huilai: 'Huilai',
    Teng_hai: 'Teng-hai',
    Hailok_hong: 'Hailok-hong',
    Nam_o: 'Nam-o',

    Teochew: 'Teochew',
    Overseas_Teochew: 'Overseas Teochew',
    Hokkien: 'Hokkien',
    Mandarin:'Mandarin',
    Cantonese: 'Cantonese',
    Hakka: 'Hakka',
    Malay: 'Malay',
    Thai: 'Thai',
    Cambodian: 'Cambodian',
    Vietnamese: 'Vietnamese',
    Singaporean: 'Singaporean',
    Indonesian: 'Indonesian',
    Japanese: 'Japanese',
    English: 'English',
    French: 'French'
};

const fr = {
    french: 'Français',
    english: 'Anglais',
    search: 'Dictionnaire',
    topicList: 'Thèmes',
    topics: 'Thèmes',
    subtopics: 'Catégories',
    words: 'Dictionnaire',
    searchScreen_label: 'Chercher dans le dictionnaire',
    topicScreen_title: 'Liste des thèmes',
    wordScreen_title: 'Liste des mots',
    copy: 'Copier',
    reportPopinTitle: 'Reporter un bug',
    definition: 'Définition',
    chinese_char: 'Chinois',
    romanization: 'Romanisation',
    partOfSpeech: 'Nature grammaticale',
    dialect: 'Dialecte',
    origin: 'Origine',
    register: 'Registre d\'expression',
    comment:'Commentaire',
    reportButton: 'Reporter',
    cancelButton: 'Annuler',
    typeOfBug: 'Type de bug',
    reported: 'Report de bug envoyé',

    noun: 'Nom',
    surname: 'Nom de famille',
    pronoun: 'Pronom',
    verb: 'Verbe',
    auxiliary_verb: 'Auxiliaire',
    adjective: 'Adjectif',
    adverb: 'Adverbe',
    number_word: 'Unité de nombre',
    measure_word: 'Unité de mesure',
    interjection: 'Interjection',
    onomatopoeia: 'Onomatopée',
    conjunction: 'Conjonction',
    preposition: 'Preposition',
    particle: 'Particule',
    expression: 'Expression',
    structure: 'Structure',
    sentence: 'Phrase',
    determiner: 'Déterminant',

    vulgar: 'Vulgaire',
    colloquial: 'Familier',
    polite: 'Courant',
    literary: 'Soutenu',
    formal: 'Formel',
    dated: 'Ancien',

    Overseas_TC: 'Teochew de la diaspora',
    MSI_TC: 'Teochew MSI',
    Malay_TC: 'Teochew malay',
    Singaporean_TC: 'Teochew singapourien',
    Indonesian_TC: 'Teochew indonésien',
    Cambodian_TC: 'Teochew cambodgien',
    Vietnamese_TC: 'Teochew vietnamien',
    Diosuan: 'Diosuan',
    Husian: 'Husian',
    Gik_ion: 'Gik-ion',
    Dio_ion: 'Dio-ion',
    Pouleng: 'Pouleng',
    Huilai: 'Huilai',
    Teng_hai: 'Teng-hai',
    Hailok_hong: 'Hailok-hong',
    Nam_o: 'Nam-o',

    Teochew: 'Teochew',
    Overseas_Teochew: 'Teochew de la diaspora',
    Hokkien: 'Hokkien',
    Mandarin:'Mandarin',
    Cantonese: 'Cantonais',
    Hakka: 'Hakka',
    Malay: 'Malais',
    Thai: 'Thaï',
    Cambodian: 'Cambodgien',
    Vietnamese: 'Vietnamien',
    Singaporean: 'Singapourien',
    Indonesian: 'Indonésien',
    Japanese: 'Japonais',
    English: 'Anglais',
    French: 'Français'
};

// Set the locale once at the beginning of your app.
//i18n.locale = Localization.locale
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;
i18n.translations = { fr, en };

class LabelService {

    getDefinitionColumn(locale){
        if(locale.startsWith('fr')) {
            return 'def_fr';
        } else if (locale.startsWith('en')) {
            return 'def_en'
        } else {
            return 'def_en';
        }
    }

    getTopicColumn(locale){
        if(locale.startsWith('fr')) {
            return 'topic_fr';
        } else if (locale.startsWith('en')) {
            return 'topic_en'
        } else {
            return 'topic_en';
        }
    }

    getSubtopicColumn(locale){
        if(locale.startsWith('fr')) {
            return 'subtopic_fr';
        } else if (locale.startsWith('en')) {
            return 'subtopic_en'
        } else {
            return 'subtopic_en';
        }
    }
}

const labelService = new LabelService();
export default labelService;
