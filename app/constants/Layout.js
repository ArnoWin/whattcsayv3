import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width,
    height,
  },
  isVerySmallDevice: width <= 325,
  isSmallDevice: width <= 360,
  isRegularDevice: width <= 393,
  isWideDevice: width > 393,
};
