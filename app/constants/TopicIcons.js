export const topicIcons = new Map();

topicIcons.set(2, require('../../assets/images/topics/2.png'));
topicIcons.set(3, require('../../assets/images/topics/3.png'));
topicIcons.set(4, require('../../assets/images/topics/4.png'));
topicIcons.set(5, require('../../assets/images/topics/5.png'));
topicIcons.set(6, require('../../assets/images/topics/6.png'));
topicIcons.set(7, require('../../assets/images/topics/7.png'));
topicIcons.set(8, require('../../assets/images/topics/8.png'));
topicIcons.set(9, require('../../assets/images/topics/9.png'));
topicIcons.set(10, require('../../assets/images/topics/10.png'));
topicIcons.set(11, require('../../assets/images/topics/11.png'));
topicIcons.set(12, require('../../assets/images/topics/12.png'));
topicIcons.set(13, require('../../assets/images/topics/13.png'));
topicIcons.set(14, require('../../assets/images/topics/14.png'));