export const map = new Map();

map.set('Overseas TC', 'Overseas_TC');
map.set('MSI-TC', 'MSI_TC');
map.set('Malay-TC', 'Malay_TC');
map.set('Singaporean-TC', 'Singaporean_TC');
map.set('Indonesian-TC', 'Indonesian_TC');
map.set('Cambodian-TC', 'Cambodian_TC');
map.set('Vietnamese-TC', 'Vietnamese_TC');
map.set('Diosuan', 'Diosuan');
map.set('Husian', 'Husian');
map.set('Gik-ion', 'Gik_ion');
map.set('Dio-ion', 'Dio_ion');
map.set('Pouleng', 'Pouleng');
map.set('Huilai', 'Huilai');
map.set('Teng-hai', 'Teng_hai');
map.set('Hailok-hong', 'Hailok_hong');
map.set('Nam-o', 'Nam_o');
