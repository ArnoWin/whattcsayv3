import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import * as React from 'react';
import Colors from '../constants/Colors';

import TabBarIcon from './TabBarIcon';
import SearchScreen from '../screens/SearchScreen';
import TopicsScreen from '../screens/TopicsScreen';
import OptionMenu from "./OptionMenu";
import { LocalizationContext } from "../../LocalizationContext";
import {Platform, View} from "react-native";


const Tab = createMaterialTopTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

const BottomTabNavigator = ({ navigation, route }) => {

    const { t } = React.useContext(LocalizationContext);

    const TAB_ICON = {
        Home: "magnify",
        Topics: "topics"
    };

    React.useEffect(() => {
        navigation.setOptions(
            {
                headerTitleAlign: 'center',
                headerTitle: getHeaderTitle(route),
                headerTitleStyle : {
                },
                headerStyle: {
                    backgroundColor: Colors.headerTabColor,
                    borderBottomLeftRadius: 50,
                    borderBottomRightRadius: 50
                },
                headerRightContainerStyle : {
                    paddingRight: Platform.OS === 'ios' ? 5 : 0
                },
                headerTintColor: Colors.white,
                headerRight: () => (
                    <OptionMenu />
                ),
                headerLeft: () => (
                    <View />
                )
            });
    });

    function getHeaderTitle(route) {
        const routeName = getFocusedRouteNameFromRoute(route) ?? INITIAL_ROUTE_NAME;

        switch (routeName) {
            case 'Home':
                return t('search');
            case 'Topics':
                return t('topics');
        }
    }

    const createScreenOptions = ({ route }) => {
        const iconName = TAB_ICON[route.name];
        return {
            tabBarIcon: ({ size, color }) => <TabBarIcon name={iconName} size={size} color={color} />,
            tabBarIconStyle : {height: 35, width: 35},
            tabBarIndicatorStyle: { borderWidth: 2, borderColor: Colors.headerTabColor},
            showIcon: true,
            tabBarActiveTintColor: Colors.headerTabColor
        };
    };

    return (
        <Tab.Navigator tabBarPosition={'bottom'}
                       screenOptions={createScreenOptions}>
            <Tab.Screen name="Home" component={SearchScreen}
                        options={({ navigation, route }) => ({
                            title: t('search'),
                            tabBarLabel:() => {return null},
                            tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="magnify"/>,
                            inactiveBackgroundColor: Colors.screenBackground
                        })}
            />
            <Tab.Screen name="Topics" component={TopicsScreen}
                        options={({ route }) => ({
                            gestureDirection: 'horizontal',
                            title: t('topics'),
                            tabBarLabel:() => {return null},
                            tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="view-grid"/>,
                        })}
            />
        </Tab.Navigator>
    );
}

export default BottomTabNavigator


