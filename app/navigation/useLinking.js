import * as Linking from 'expo-linking';
import useLinking from "@react-navigation/native/src/useLinking";

export default function(containerRef) {
    return useLinking(containerRef, {
        prefixes: [Linking.makeUrl('/')],
        config: {
            Root: {
                path: 'root',
                screens: {
                    Home: 'home',
                    Topics: 'topics',
                    Words: 'words',
                    Settings: 'settings',
                },
            },
        },
    });
}
