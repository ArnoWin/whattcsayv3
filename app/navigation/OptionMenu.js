import {Ionicons} from "@expo/vector-icons";
import * as React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from "react-native-popup-menu";
import Colors from "../constants/Colors";
import { LocalizationContext } from "../../LocalizationContext";

const OptionMenu = () => {

    const { t, locale, setLocale } = React.useContext(LocalizationContext);

    return (
        <View>
            <Menu>
                <MenuTrigger>
                    <Ionicons
                        style={{paddingRight: 15}}
                        name='ios-arrow-down'
                        size={30}
                        color='white'
                    />
                </MenuTrigger>
                <MenuOptions optionsContainerStyle={styles.menuOptionsSytle}>
                    <MenuOption onSelect={() => setLocale('fr')}>
                        <Text style={styles.menuOptionText}>{t('french')}</Text>
                    </MenuOption>
                    <View style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: Colors.defaultBackground,
                            marginHorizontal: 2
                        }}/>
                    <MenuOption onSelect={() => setLocale('en')}>
                        <Text style={styles.menuOptionText}>{t('english')}</Text>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        </View>
    );
}

export default OptionMenu

const styles = StyleSheet.create({
    menuOptionsSytle : {
        width: 100,
        opacity: 0.5,
        borderRadius: 8
    },
    menuOptionText : {
        paddingVertical: 10,
        textAlign: 'center',
        fontFamily: 'nunito-regular',
        fontWeight: '400',
        fontSize: 14,
        color: Colors.lightGrey
    }
});
